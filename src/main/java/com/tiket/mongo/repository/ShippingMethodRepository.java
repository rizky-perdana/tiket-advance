package com.tiket.mongo.repository;

import com.tiket.mongo.collection.ShippingMethods;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ShippingMethodRepository extends MongoRepository<ShippingMethods, Long> {
}
