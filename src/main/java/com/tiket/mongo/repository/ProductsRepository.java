package com.tiket.mongo.repository;

import com.tiket.mongo.collection.Products;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductsRepository extends MongoRepository<Products, Long> {
}
