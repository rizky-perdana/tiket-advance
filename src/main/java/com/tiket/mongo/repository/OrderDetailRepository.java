package com.tiket.mongo.repository;

import com.tiket.mongo.collection.OrderDetails;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OrderDetailRepository extends MongoRepository<OrderDetails, Long> {

}
