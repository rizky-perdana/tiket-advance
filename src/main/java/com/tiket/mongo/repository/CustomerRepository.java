package com.tiket.mongo.repository;

import com.tiket.mongo.collection.Customers;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CustomerRepository  extends MongoRepository<Customers, Long>{
}
