package com.tiket.mongo.repository;

import com.tiket.mongo.collection.Employees;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface EmployeeRepository extends MongoRepository<Employees, Long> {
}
