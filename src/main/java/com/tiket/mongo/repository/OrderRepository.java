package com.tiket.mongo.repository;

import com.tiket.mongo.collection.Orders;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OrderRepository extends MongoRepository<Orders, Long> {

}
