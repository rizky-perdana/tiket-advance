package com.tiket.mongo.controller;

import com.tiket.mongo.collection.Customers;
import com.tiket.mongo.collection.Employees;
import com.tiket.mongo.collection.OrderDetails;
import com.tiket.mongo.collection.Orders;
import com.tiket.mongo.collection.Products;
import com.tiket.mongo.collection.ShippingMethods;
import com.tiket.mongo.service.CsvDataLoader;
import com.tiket.mongo.service.CustomerService;
import com.tiket.mongo.service.EmployeeService;
import com.tiket.mongo.service.OrderDetailService;
import com.tiket.mongo.service.OrderService;
import com.tiket.mongo.service.ProductService;
import com.tiket.mongo.service.ShippingMethodService;
import com.tiket.mongo.util.CsvPath;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CsvController {
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private ProductService prodService;
	
	@Autowired
	private ShippingMethodService smService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private OrderDetailService orderDetailService;
	
	@Autowired
	private CsvDataLoader loader;
	
	@GetMapping("/uploadCustomerCSV")
	public void uploadCustomerCSV() {
		List<Customers> customers = loader.readCsvCustomerMethods(CsvPath.CUSTOMERS);
		customerService.uploadCustomer(customers);
	}
	
	@GetMapping("/uploadEmployeeCSV")
	public void uploadEmployeeCSV() {
		List<Employees> employees = loader.readCsvEmployees(CsvPath.EMPLOYEES);
		employeeService.uploadEmployee(employees);
	}
	
	@GetMapping("/uploadProductCSV")
	public void uploadProductCSV() {
		List<Products> products = loader.readCsvProducts(CsvPath.PRODUCTS);
		prodService.uploadProduct(products);
	}
	
	@GetMapping("/uploadShippingMethodCSV")
	public void uploadShippingMethodCSV() {
		List<ShippingMethods> sm = loader.readCsvShippingMethods(CsvPath.SHIPPINGMETHODS);
		smService.uploadShippingMethod(sm);
	}
	
	@GetMapping("/uploadOrderCSV")
	public void uploadOrderCSV() throws ParseException {
		List<Orders> order = loader.readCsvOrder(CsvPath.ORDERS);
		orderService.uploadOrder(order);
	}
	
	@GetMapping("/uploadOrderDetailCSV")
	public void uploadOrderDetailCSV() throws ParseException {
		List<OrderDetails> order = loader.readCsvOrderDetail(CsvPath.ORDERDETAILS);
		orderDetailService.uploadOrderDetail(order);
	}
}
