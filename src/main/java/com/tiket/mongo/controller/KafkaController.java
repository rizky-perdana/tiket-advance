package com.tiket.mongo.controller;

import com.tiket.mongo.collection.Customers;
import com.tiket.mongo.kafka.ProduceService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/kafka")
public class KafkaController {
	
	@Autowired
	private ProduceService produceService ;
	
	@GetMapping("customers")
	public List<Customers> getAllCustomer(){
		return produceService.getListCustomer();
	}
	
	@GetMapping("employeeByorder")
	public List<Object> getEmployeeByOrder(){
		return produceService.getEmployeeByOrder();
	}
	
	@GetMapping("shippingMethodByOrder")
	public List<Object> getSmInUsed(){
		return produceService.getShippingMethodInUsed();
	}
	
	@RequestMapping(value = "getProductDetail/{orderId}", method = RequestMethod.GET)
	public List<Object> getProductByOrder(@PathVariable("orderId") long orderID){
		return produceService.getProductByOrder(orderID);
	}
	
	@RequestMapping(value = "getDetailPayment/{orderId}", method = RequestMethod.GET)
	public List<Object> getDetailPayment(@PathVariable("orderId") long orderID){
		
		return produceService.getDetailPayment(orderID);
	}
	
}
