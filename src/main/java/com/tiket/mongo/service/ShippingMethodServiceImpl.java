package com.tiket.mongo.service;

import com.tiket.mongo.collection.ShippingMethods;
import com.tiket.mongo.repository.ShippingMethodRepository;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShippingMethodServiceImpl implements ShippingMethodService {

    @Autowired
    private ShippingMethodRepository smRepo;

    @Override
    public void uploadShippingMethod(List<ShippingMethods> shippingMethods) {
        for (ShippingMethods shippingSingle : shippingMethods) {
            smRepo.save(shippingSingle);
        }

    }

    @Override
    public ShippingMethods findShippingMethod(Long shippingMethod) {
        ShippingMethods sm = smRepo.findById(shippingMethod).get();
        return sm;
    }

}
