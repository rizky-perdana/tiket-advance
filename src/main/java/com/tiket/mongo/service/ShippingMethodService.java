package com.tiket.mongo.service;

import com.tiket.mongo.collection.ShippingMethods;
import java.util.List;

public interface ShippingMethodService {
	public void uploadShippingMethod(List<ShippingMethods> shippingMethods);
	public ShippingMethods findShippingMethod(Long shippingMethod);
}
