package com.tiket.mongo.service;

import com.tiket.mongo.collection.Orders;
import com.tiket.mongo.repository.OrderRepository;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderRepository orderRepository;
	
	@Override
	public void uploadOrder(List<Orders> order) {
            order.forEach((orders) -> {
                orderRepository.save(orders);
            });
		
	}

	@Override
	public Orders findOrder(Long orderID) {
		Orders od = orderRepository.findById(orderID).get();
		return od;
	}

}
