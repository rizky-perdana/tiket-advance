package com.tiket.mongo.service;

import com.tiket.mongo.collection.Orders;
import java.util.List;

public interface OrderService {
	public void uploadOrder(List<Orders> order);
	public Orders findOrder(Long orderID);
}
