package com.tiket.mongo.service;

import com.tiket.mongo.collection.Customers;
import java.util.List;

public interface CustomerService {
	public void uploadCustomer(List<Customers> customers);
	public Customers findCustomer(Long customerID);
}
