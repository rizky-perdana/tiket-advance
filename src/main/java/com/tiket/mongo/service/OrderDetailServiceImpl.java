package com.tiket.mongo.service;

import com.tiket.mongo.collection.OrderDetails;
import com.tiket.mongo.repository.OrderDetailRepository;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderDetailServiceImpl implements OrderDetailService {

	@Autowired
	private OrderDetailRepository orderDetailRepository;
	
	@Override
	public void uploadOrderDetail(List<OrderDetails> ods) {
		for (OrderDetails orderDetails : ods) {
			orderDetailRepository.save(orderDetails);
		}	
	}

}
