package com.tiket.mongo.service;

import com.tiket.mongo.collection.Customers;
import com.tiket.mongo.repository.CustomerRepository;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;
	
	@Override
	public void uploadCustomer(List<Customers> customers) {
		for (Customers customer : customers) {
			customerRepository.save(customer);
		}
	}

	@Override
	public Customers findCustomer(Long customerID) {
		Customers customer = customerRepository.findById(customerID).get();
		return customer;
	}

}
