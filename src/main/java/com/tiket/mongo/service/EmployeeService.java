package com.tiket.mongo.service;

import com.tiket.mongo.collection.Employees;
import java.util.List;

public interface EmployeeService {
	public void uploadEmployee(List<Employees> employees);
	public Employees findEmployee(Long employeeID);
}
