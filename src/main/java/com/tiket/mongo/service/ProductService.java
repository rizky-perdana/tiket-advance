package com.tiket.mongo.service;

import com.tiket.mongo.collection.Products;
import java.util.List;

public interface ProductService {
	public void uploadProduct(List<Products> products);
	public Products findProduct(Long productID);
}
