package com.tiket.mongo.service;

import com.tiket.mongo.collection.Employees;
import com.tiket.mongo.repository.EmployeeRepository;
import com.tiket.mongo.service.EmployeeService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public void uploadEmployee(List<Employees> employees) {
		for (Employees employee : employees) {
			employeeRepository.save(employee);
		}
		
	}

	@Override
	public Employees findEmployee(Long employeeID) {
		Employees emp = employeeRepository.findById(employeeID).get();
		return emp;
	}

}
