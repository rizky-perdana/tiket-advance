package com.tiket.mongo.service;

import com.tiket.mongo.collection.OrderDetails;
import java.util.List;

public interface OrderDetailService {
	public void uploadOrderDetail (List<OrderDetails> ods);
}
