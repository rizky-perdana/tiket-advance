package com.tiket.mongo.service;

import com.tiket.mongo.collection.Products;
import com.tiket.mongo.repository.ProductsRepository;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductsRepository productRepository;
	
	@Override
	public void uploadProduct(List<Products> products) {
            products.forEach((product) -> {
                productRepository.save(product);
            });
		
	}

	@Override
	public Products findProduct(Long productID) {
		Products prod = productRepository.findById(productID).get();
		return prod;
	}

}
