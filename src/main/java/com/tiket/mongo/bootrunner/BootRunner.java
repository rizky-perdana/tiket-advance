package com.tiket.mongo.bootrunner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootRunner {

	public static void main(String[] args) throws InterruptedException {
		SpringApplication.run(BootRunner.class, args);
	}
}
