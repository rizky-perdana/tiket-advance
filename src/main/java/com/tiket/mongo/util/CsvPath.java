package com.tiket.mongo.util;

public abstract class CsvPath {
	public static final String PRODUCTS = "src/main/resources/Products.csv";
	public static final String EMPLOYEES = "src/main/resources/Employees.csv";
	public static final String CUSTOMERS = "src/main/resources/Customers.csv";
	public static final String ORDERS = "src/main/resources/Orders.csv";
	public static final String ORDERDETAILS = "src/main/resources/OrderDetails.csv";
	public static final String SHIPPINGMETHODS = "src/main/resources/ShippingMethods.csv";
}
