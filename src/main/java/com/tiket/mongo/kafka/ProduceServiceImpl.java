package com.tiket.mongo.kafka;

import com.tiket.mongo.collection.Customers;
import com.tiket.mongo.collection.Employees;
import com.tiket.mongo.collection.OrderDetails;
import com.tiket.mongo.collection.Orders;
import com.tiket.mongo.collection.Products;
import com.tiket.mongo.collection.ShippingMethods;
import com.tiket.mongo.repository.CustomerRepository;
import com.tiket.mongo.repository.OrderDetailRepository;
import com.tiket.mongo.repository.OrderRepository;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ProduceServiceImpl extends DetailPaymentService implements ProduceService {

    @Autowired
    private KafkaTemplate<String, Customers> customerkafkaTemplate;

    @Autowired
    private KafkaTemplate<String, List<Object>> objectkafkaTemplate;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private OrderRepository orderRepo;

    @Autowired
    private OrderDetailRepository odRepo;

    @Value("${message.topic.name}")
    private String kafkaTopic;

    @Override
    public List<Customers> getListCustomer() {
        List<Customers> listCustomer = customerRepository.findAll();
        for (Customers customers : listCustomer) {
            customerkafkaTemplate.send(kafkaTopic, customers);
        }
        return listCustomer;
    }

    @Override
    public List<Object> getShippingMethodInUsed() {
        List<Object> listSm = new ArrayList<>();
        List<Orders> orders = orderRepo.findAll();
        orders.stream().map((order) -> {
            Map<String, Object> map = new HashMap<>();
            ShippingMethods sm = order.getShippingMethods();
            map.put("OrderID", order.get_id());
            map.put("ShippingMethodID", sm.get_id());
            map.put("ShippingMethod", sm.getShippingMethod());
            return map;
        }).forEachOrdered((map) -> {
            listSm.add(map);
        });
        objectkafkaTemplate.send(kafkaTopic, listSm);
        return listSm;

    }

    @Override
    public List<Object> getEmployeeByOrder() {
        List<Object> listEmp = new ArrayList<>();

        List<Orders> orders = orderRepo.findAll();
        orders.stream().map((order) -> {
            Map<String, Object> map = new HashMap<>();
            Employees emp = order.getEmployee();
            map.put("EmployeeID", emp.get_id());
            map.put("FirstName", emp.getFirstName());
            map.put("LastName", emp.getLastName());
            map.put("Title", emp.getTitle());
            map.put("WorkPhone", emp.getWorkPhone());
            map.put("OrderID", order.get_id());
            return map;
        }).forEachOrdered((map) -> {
            listEmp.add(map);
        });
        objectkafkaTemplate.send(kafkaTopic, listEmp);
        return listEmp;
    }

    @Override
    public List<Object> getProductByOrder(Long orderID) {
        List<Object> listProd = new ArrayList<>();
        List<OrderDetails> orderDetail = odRepo.findAll();
        orderDetail.stream().filter((od) -> (Objects.equals(od.getOrder().get_id(), orderID))).map((od) -> {
            Map<String, Object> map = new HashMap<>();
            Products prod = od.getProduct();
            Orders order = od.getOrder();
            map.put("OrderID", order.get_id());
            map.put("OrderDetailID", od.get_id());
            map.put("ProductName", prod.getProductName());
            map.put("Quantity", od.getQuantity());
            map.put("UnitPrice", "$" + od.getUnitPrice());
            map.put("Discount", "$" + od.getDiscount());
            Long disc = od.get_id();
            if (disc != 0) {
                Double subTotal = od.getUnitPrice() * disc / 100;
                map.put("SubTotal", "$" + subTotal);
            } else {
                map.put("SubTotal", "$" + od.getUnitPrice());
            }
            return map;
        }).forEachOrdered((map) -> {
            listProd.add(map);
        });
        objectkafkaTemplate.send(kafkaTopic, listProd);
        return listProd;
    }

    @Override
    public List<Object> getDetailPayment(Long orderID) {
        List<Object> obj = getDetailPaymentQuery(orderID);
        objectkafkaTemplate.send(kafkaTopic, obj);
        return obj;
    }

}
