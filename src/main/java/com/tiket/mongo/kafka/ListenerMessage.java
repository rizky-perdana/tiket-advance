package com.tiket.mongo.kafka;

import com.tiket.mongo.collection.Customers;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;

public class ListenerMessage {

    private CountDownLatch cdl = new CountDownLatch(1);

    @Value("${message.topic.name}")
    private String kafkaTopic;

    @KafkaListener(topics = "${message.topic.name}")
    public void receiveCustomer(Customers sm) {
        cdl.countDown();
        System.out.println(sm.toString());
    }

    @KafkaListener(topics = "${message.topic.name}")
    public void receiveObject(List<Object> obj) {
        cdl.countDown();
        System.out.println(obj.toString());
    }
}
