package com.tiket.mongo.kafka;

import com.tiket.mongo.collection.Customers;
import java.util.List;

public interface ProduceService {
	public List<Object> getShippingMethodInUsed();

	public List<Customers> getListCustomer();
	
	public List<Object> getEmployeeByOrder();
	
	public List<Object> getProductByOrder(Long orderID);
	
	public List<Object> getDetailPayment(Long orderID);
}
